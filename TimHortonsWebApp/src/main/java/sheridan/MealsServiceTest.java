package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class MealsServiceTest {

	@Test
	public void testDrinksRegular() {
		
		List<String> mealList =  MealsService.getAvailableMealTypes(MealType.DRINKS);
		
		assertTrue("meallist not empty for drink type", !mealList.isEmpty());
		
	}
	
	@Test
	public void testDrinksException() {
		
		List<String> mealList =  MealsService.getAvailableMealTypes(null);
		
		assertTrue("No brands Available for null meal type", mealList.get(0).equals("No Brand Available"));
		
	}
	
	
	@Test
	public void testDrinksBoundaryIn() {
		
		List<String> mealList =  MealsService.getAvailableMealTypes(MealType.DRINKS);
		
		assertTrue("mealList has more than 3 elements for drink meal type", mealList.size() > 3);
		
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		
		List<String> mealList =  MealsService.getAvailableMealTypes(null);
		
		assertTrue("mealList has only 1 element when meal type is null", mealList.size() == 1);
		
	}
	
	
	
}
